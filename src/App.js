import React from 'react';
import logo from './logo.svg';
import { Button, Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import './App.css';

function App() {
  return (
    <div className="App">
      {/* <header className="App-header"> 
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <div>
        <Button type="primary">Primary Button</Button>
        <Button>Default Button</Button>
        <Button type="dashed">Dashed Button</Button>
        <br />
        <Button type="text">Text Button</Button>
        <Button type="link">Link Button</Button>
      </div>
       <div>
        <Tooltip title="search">
          <Button type="primary" shape="circle" icon={<SearchOutlined />} />
        </Tooltip>
        <Button className = "add" type="primary" shape="circle">
          A
        </Button>
        <Button type="primary" icon={<SearchOutlined />}>
          Search
        </Button>
        <Tooltip title="search">
          <Button shape="circle" icon={<SearchOutlined />} />
        </Tooltip>
        <Button icon={<SearchOutlined />}>Search</Button>
        <br />
        <Tooltip title="search">
          <Button shape="circle" icon={<SearchOutlined />} />
        </Tooltip>
        <Button icon={<SearchOutlined />}>Search</Button>
        <Tooltip title="search">
          <Button type="dashed" shape="circle" icon={<SearchOutlined />} />
        </Tooltip>
        <Button type="dashed" icon={<SearchOutlined />}>
          Search
    </Button>
      </div>



    </div>
  );
}
// import React, { useState } from 'react';

// const HelloMessage = () => {
//  const [name,setName] = useState("skkskk");
 
//  return(
//   <div>  Hello {name}
//       </div> )
// }

// function onChange1(value) {
//   console.log('changed', value);
  
// }
// function A(){
//   return(
//     <div className = "Aaaa">
//       <InputNumber  defaultValue={5} onChange={onChange} /> 
//     </div>
//   );
// }



// const App = () => (
//   <div className="App">

//   </div>
// );

export default App



