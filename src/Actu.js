import React, { useState } from 'react';
import { Input, Button } from 'antd';
import { PlusOutlined, MinusOutlined, CloseOutlined } from '@ant-design/icons';
import './Actu.css';


const Actu = () => {

  const [number, setNumber] = useState('0');//数字
  const [storedNumber, setStoredNumber] = useState([]);//存储数字
  const [functionType, setFunctionType] = useState('');//计算类型
  const [cont, setCont] = useState(""); //存放第一次输入的值
  // console.log(cont)

  //取相反数
  const handleToggleNegative = () => {
    if (number) {
      if (number > 0) {
        setNumber(`-${number}`);
        console.log(number)
        console.log(`${number}-`)
        setCont(`-${number}`);
      } else {
        const positiveNumber = Math.abs(parseInt(number));
        setNumber(positiveNumber);
      }
    }
  }

  // 当点击等于时计算
  const doMath = () => {
    let n = eval(cont + functionType + number)
    if (parseInt(n) < 0) {
      n = (`${Math.abs(n)}-`);
    }
    return n;

  }
  // 点击数字按钮时传出数据
  const cal = (storedNumber = []) => {
    let num = 0;
    // console.log(storedNumber)
    storedNumber.map((item, index) => {
      num += item * Math.pow(10, storedNumber.length - 1 - index)
    })
    return num;
  }

  //点击按钮时颜色发生改变
  //TODO
  function SupendButtonClick(obj) {
    //清空其它同类按钮选中颜色
    // $('div[id^="SupendButton-"]').css("background-color", "#4cb0f9");//按钮原来颜色
    //点击后变色
    // $(obj).css("background-color", "red");
  }

  return (
    <div className="nav">

      <div className="header">
        <div className="icons">
          <span className="icons-i icons-close"><CloseOutlined /></span>
          <span className="icons-i icons-minus"><MinusOutlined /></span>
          <span className="icons-i icons-plus" ><PlusOutlined /></span>
        </div>
        <Input id="input" className="input" readOnly
          value={number} onChange={() => { }}></Input>
      </div >

      <div className="btn">
        <Button title="清除（Esc）；全部清除（Option-Esc）" className="btn-i"
          onClick={() => {
            storedNumber.splice(0, storedNumber.length)
            setCont(0)
            setNumber(0);
            // console.log(storedNumber)
            // console.log(cont)
            // console.log(number)
          }}>AC</Button>

        <Button title="对所显示的值取负（或按下Option- 减号键[-]）" className="btn-i btn-f" 
        style={{ fontWeight: "bold" }} onClick={() => {
          // console.log(functionType)
          handleToggleNegative()
          // console.log(typeof cont)
          // setNumber('0');
        }}>+/-</Button>

        <Button title="百分比（或按下%键）" className="btn-i"
          onClick={() => {
            setNumber(number / 100)
          }}>%</Button>

        <Button title="除（或按下/键）" className="btn-ye" onClick={() => {
          let functionType = "/";
          // console.log(functionType)
          setFunctionType(functionType);
          setCont(number)
          storedNumber.splice(0, storedNumber.length)
          // console.log(cont)
        }}>÷</Button>

        <Button onClick={() => {
          storedNumber.push(7)
          setNumber(cal(storedNumber));
        }} >7</Button>

        <Button onClick={() => {
          storedNumber.push(8)
          setNumber(cal(storedNumber));
        }} >8</Button>

        <Button onClick={() => {
          storedNumber.push(9)
          setNumber(cal(storedNumber));
        }} >9</Button>

        <Button title="乘（或按下*键）" className="btn-ye" onClick={() => {
          let functionType = "*";
          // console.log(functionType)
          setFunctionType(functionType);
          storedNumber.splice(0, storedNumber.length)
          // setCont(number)
          // console.log(cont)
        }}>x</Button>

        <Button onClick={() => {
          storedNumber.push(4)
          setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>4</Button>

        <Button onClick={() => {
          storedNumber.push(5)
          setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>5</Button>

        <Button onClick={() => {
          storedNumber.push(6)
          setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>6</Button>

        <Button title="减（或按下-键）" className="btn-ye" onClick={() => {
          let functionType = "-";
          // console.log(functionType)
          setFunctionType(functionType);
          setCont(number)
          storedNumber.splice(0, storedNumber.length)
          // console.log(cont)
        }}>-</Button>

        <Button onClick={() => {
          storedNumber.push(1)
          setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>1</Button>

        <Button onClick={() => {
          storedNumber.push(2)
          setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>2</Button>

        <Button onClick={() => {
          storedNumber.push(3)
          setNumber(cal(storedNumber));
          // console.log(number)
          // setStoredNumber(storedNumber)
        }}>3</Button>

        <Button title="加（或按下+键）" className="btn-ye " onClick={() => {
          let functionType = "+";
          // doInput()
          // console.log(functionType)
          setFunctionType(functionType);
          setCont(number)
          storedNumber.splice(0, storedNumber.length)
          // setNumber.splice(0, storedNumber.length)
          // console.log(cont)
          // setNumber('0');
        }}>+</Button>

        <Button className="btn-o btn-btmr" onClick={() => {
          // handleSetDisplayValue("0")
          storedNumber.push(0)
          setNumber(cal(storedNumber));
          // storedNumber.push(0)
          // setNumber(cal(storedNumber));
          // setStoredNumber(storedNumber)
        }}>0</Button>

        <Button onClick={() => {
          // let num = "."
          // handleSetDisplayValue(num); 
          // storedNumber.push(".")
          // console.log(handleSetDisplayValue(num))
          // console.log(typeof handleSetDisplayValue(num))

          // // let number = ["."]
          // setNumber(cal(storedNumber));
          // console.log(number)
          // setStoredNumber(storedNumber)
        }}>.</Button>
        <Button title="等于（或按下enter键）" className="btn-ye  btn-btml"
          onClick={() => {
            console.log(doMath())
            setNumber(doMath())
            setCont(doMath())
            // setFunctionType(functionType);
          }}>=</Button>
      </div>
    </div>
  )
}

export default Actu